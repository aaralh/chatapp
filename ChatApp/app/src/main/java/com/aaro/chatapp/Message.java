package com.aaro.chatapp;

/**
 * Created by Aaro on 24.3.2018.
 */

public class Message {

    private String sender;
    private int timestamp;
    private String content;

    public Message(String sender, int timestamp, String content) {
        this.sender = sender;
        this.timestamp = timestamp;
        this.content = content;
    }
}

